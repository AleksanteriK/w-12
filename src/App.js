import logo from './logo.svg';
import './App.css';
import {Card} from 'react-bootstrap';

function App() {
  return (
    <div className="App">
      <Card>
      <img
        src={'https://www.cc.puv.fi/~e2202945/sources/logo.png'}
        alt="Logo"
        className="img-fluid"
        style={{
        maxWidth: '100%', // Default size for larger screens
        maxHeight: '80%', // Default size for larger screens
        width: '10%', // Adjust as needed for smaller screens
        height: '10%', // Adjust as needed for smaller screens
        border: '5px solid'
      }}/>
      <div>
        <span style={{fontSize: '25px'}}>Aleksanteri Koivisto</span>
      </div>
      <div>
        <span style={{fontSize: '18px'}}>e2202945@edu.vamk.fi</span>
      </div>
      <div>
        <span style={{fontSize: '20px'}}>Opiskelija</span>
      </div>
      <div>
        <span style={{fontSize: '15px'}}>+358 44 25 30 151</span>
      </div>
      <div>
        <span style={{fontSize: '19px'}}>Wolffintie 30, VAASA, Finland</span>
      </div>
      </Card>
    </div>
  );
}

export default App;
